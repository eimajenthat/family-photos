<?php

include_once __DIR__.'/../src/bootstrap.php';

use function Stringy\create as s;

$age = isset($argv[1]) ? $argv[1] : 2;

// Find everything modified within the last $age days, default is 2
echo "Checking for media files modified in the last $age days...\n";
$files = array_filter(explode("\n",`find $config[album_library] -mtime -$age`));

echo "Generating updated thumbnails where needed...\n";
foreach($files as $f) {
    $thumb_path = str_replace($config['album_library'],$config['thumb_library'],$f).'-thumb.jpg';
    if(!file_exists($thumb_path) || !is_readable($thumb_path)
            || stat($thumb_path)['mtime'] <= stat($f)['mtime']) {
        try {
            echo $thumb_path."\n";
            generateThumbnail($f, $thumb_path);
        } catch(Exception $e) {
            echo $e->getMessage();
        }
    }
}
echo "DONE\n";
