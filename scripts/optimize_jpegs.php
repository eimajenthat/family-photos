<?php

include_once __DIR__.'/../src/bootstrap.php';

$age = isset($argv[1]) ? $argv[1] : 2;

// Find all jpg files modified within the last $age days, default is 2
$files = array_filter(explode("\n",`find $config[album_library] -mtime -$age -iname "*.jpg"`));

foreach($files as $f) {
    echo shell_exec('jpegoptim -p -P '.escapeshellarg($f));
}

// Do the same thing for the thumbnail directory
$files = array_filter(explode("\n",`find $config[thumb_library] -mtime -$age -iname "*.jpg"`));

foreach($files as $f) {
    echo shell_exec('jpegoptim --strip-none -p -P '.escapeshellarg($f));
}
