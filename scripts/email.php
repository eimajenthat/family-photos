<?php

// send out email update for the month

include_once __DIR__.'/../src/bootstrap.php';

// check to see if there are at least 4 images in the current month's directory
$dt=date_create('first day of last month');
$new_pics = listDir($config['album_library'].'/'.$dt->format('Y-m'));
if(count($new_pics) < 5) {
    die('Not enough pictures');
}

if(isset($argv[1]) && $argv[1] == 'live') {
    // if live, load email addresses from flat file
    $recipients = array_map('trim', file($config['email']['recipients_list']));
} else {
    // otherwise, pull sender address from config
    $recipients = [empty($config['email']['from_reply_to']) ? $config['address']['from_reply_to'] : $config['email']['from_reply_to'] ];
}

// setup mailer
$transport = (new Swift_SmtpTransport($config['email']['smtp_host'], $config['email']['smtp_port']))
    ->setUsername($config['email']['smtp_username'])
    ->setPassword($config['email']['smtp_password']);
$mailer = new Swift_Mailer($transport);

$message = (new Swift_Message('Adams Family Photo Site '.$dt->format('F').' '.$dt->format('Y').' Update'))
    ->setFrom([$config['email']['from_address'] => $config['email']['from_name']]);
if($config['email']['from_reply_to']) {
    $message->setReplyTo($config['email']['from_reply_to']);
}

// fill out template
$template = <<<BYE
<p>
    We have posted some new photos to our family photo site for %s %s.  Check them out!
</p>
<p>
    <img style="width:600px" src="%s"/><br/>
    <img style="width:600px" src="%s"/><br/>
    <img style="width:600px" src="%s"/><br/>
    <img style="width:600px" src="%s"/><br/>
    <img style="width:600px" src="%s"/>
</p>
<p>
    There are even more on the web site.  <a href="%s">Click here</a> to see them.
</p>
<p>
    To access the photo site, you will need to enter the following username and password.<br/>
    Username: %s<br/>
    Password: %s
</p>
BYE;
$message->setBody(sprintf($template, 
    $dt->format('Y'),
    $dt->format('F'),
    $message->embed(Swift_Image::fromPath($config['album_library'].'/'.$dt->format('Y-m').'/'.$new_pics[0])),
    $message->embed(Swift_Image::fromPath($config['album_library'].'/'.$dt->format('Y-m').'/'.$new_pics[1])),
    $message->embed(Swift_Image::fromPath($config['album_library'].'/'.$dt->format('Y-m').'/'.$new_pics[2])),
    $message->embed(Swift_Image::fromPath($config['album_library'].'/'.$dt->format('Y-m').'/'.$new_pics[3])),
    $message->embed(Swift_Image::fromPath($config['album_library'].'/'.$dt->format('Y-m').'/'.$new_pics[4])),
    $config['url'].'/?path=/'.$dt->format('Y-m'),
    $config['username'],
    $config['password']
), 'text/html');

// send
foreach($recipients as $to) {
    $message->setTo($to);
    $result = $mailer->send($message);
}
