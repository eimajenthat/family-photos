#!/bin/bash

USER="root"
HOST="photos.adamsnet.info"
DEST="/var/www/photos.adamsnet.info"

DIR=`dirname $0`
DIR="$DIR/../"

[[ $1 != "go" ]] && go="--dry-run" || go=""

rsync $go -az --force --delete --progress --exclude-from=$DIR/rsync_exclude.txt -e "ssh -p22" $DIR $USER@$HOST:$DEST

if [[ $1 = "go" ]]
then
    ssh $USER@$HOST "chown -R www-data $DEST && chgrp -R www-data $DEST"
fi
