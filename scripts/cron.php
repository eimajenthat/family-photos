<?php require_once __DIR__.'/../vendor/autoload.php';

use GO\Scheduler;

// Create a new scheduler
$scheduler = new Scheduler();

// Scheduler functions: https://github.com/peppeocchi/php-cron-scheduler#schedules-execution-time
// Crontab formatting: https://crontab.guru/
$scheduler->php(__DIR__.'/email.php')->at('0 8 15 * *')->output([__DIR__.'/../data/logs/email.log']);
$scheduler->php(__DIR__.'/autorotate.php')->at('0 */6 * * *')->output([__DIR__.'/../data/logs/autorotate.log']);
$scheduler->php(__DIR__.'/thumbs.php')->at('15 */6 * * *')->output([__DIR__.'/../data/logs/thumbs.log']);
$scheduler->php(__DIR__.'/thumbs.php', null, ['40000' => null])->at('15 1 * * 0')->output([__DIR__.'/../data/logs/thumbs.log']);
$scheduler->php(__DIR__.'/optimize_jpegs.php')->at('0 1 * * *')->output([__DIR__.'/../data/logs/optimize_jpegs.log']);

// Let the scheduler execute jobs which are due.
$scheduler->run();
