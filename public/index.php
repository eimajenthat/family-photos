<?php
include_once __DIR__.'/../src/bootstrap.php';

use function Stringy\create as s;

include __DIR__.'/../src/security.php';

// Controller
$library = realpath($config['album_library']);
$path = realpath($library . '/' . (isset($_GET['path'])?$_GET['path']:''));

if(!s($path)->startsWith($library)) {
    die('Path must be in your library.');
}
$path_chunk = s($path)->removeLeft($library);
if(is_dir($path)) {
    $view = 'folder';
    $files = listDir($path);
    array_walk($files, function(&$f) use ($path_chunk) { $f = $path_chunk . '/' . $f; });
} else {
    $view = 'file';
    $folder = dirname($path_chunk);
    $files = listDir(dirname($path));
    array_walk($files, function(&$f) use ($folder) { $f = $folder . '/' . $f; });
    $prev = $folder;
    $next = $folder;
    $found = false;
    foreach($files as $f) {
        if($found) {
            $next = $f;
            break;
        }
        if($f == $path_chunk) {
            $found = true;
        } else {
            $prev = $f;
        }
    }
}

// View
include __DIR__.'/../src/view/layout/_header.phtml';
include __DIR__.'/../src/view/'.$view.'.phtml';
include __DIR__.'/../src/view/layout/_footer.phtml';
