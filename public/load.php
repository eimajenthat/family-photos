<?php
include_once __DIR__.'/../src/bootstrap.php';

use function Stringy\create as s;

include __DIR__.'/../src/security.php';

$library = realpath($config['album_library']);
$path_chunk = isset($_GET['path']) ? $_GET['path'] : '';
$path = realpath($library . '/' . $path_chunk);

if (!s($path)->startsWith($library)) {
    die('Path must be in your library.');
}
if(isset($_GET['thumb']) && $_GET['thumb']) {
    $thumb_library = realpath($config['thumb_library']);
    // You can't realpath the thumb_path, because it might not exist.
    $thumb_path = $thumb_library . '/' . $path_chunk . '-thumb.jpg';
    // We've already checked the path against the library, so this shouldn't be a problem
    // but better safe than sorry.
    if(!file_exists($thumb_path) || !is_readable($thumb_path)
            || stat($thumb_path)['mtime'] <= stat($path)['mtime']) {
        generateThumbnail($path, $thumb_path);
    }

    $path = $thumb_path;
}

if(isset($_GET['download']) && $_GET['download']) {
    header('Content-Disposition: attachment; filename='.basename($path));
    header("Pragma: public");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
}
header("Content-Type: ".mime_content_type($path));

//@TODO: This page shows how to setup resumable downloads
//http://www.media-division.com/the-right-way-to-handle-file-downloads-in-php/

set_time_limit(0);
$file = @fopen($path,"rb");
if(is_resource($file)) {
    while(!feof($file)) {
        print(@fread($file, 1024*8));
        ob_flush();
        flush();
    }
}
exit;
