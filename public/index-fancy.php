<?php

// This is the beginning of an attempt to implement Slim, Plates, and some other
// third-party libraries, to make the app a bit more robust.  However, it's not
// complete.  I decided it would be better to start with something simpler.  So
// that's what you see in index.php.
die('This is not ready to use.');

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

$app = new \Slim\App;

// Fetch DI Container
$container = $app->getContainer();

$container = new Container([
    'settings' => [
        'view' => [
            'directory'     => '',
            'assetPath'     => '',
            'fileExtension' => 'phtml',
        ],
        'data' => '',
    ],
]);

// Register Plates View helper:
// Option 1, using PlatesProvider
$container->register(new \Projek\Slim\PlatesProvider);

$app->get('/', function (Request $request, Response $response) {
    return $this->view->render('profile', [
        'name' => $args['name']
    ]);
});
$app->get('/hello/{name}', function (Request $request, Response $response) {
    $name = $request->getAttribute('name');
    $response->getBody()->write("Hello, $name");

    return $response;
});
$app->run();
