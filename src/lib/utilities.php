<?php

use Intervention\Image\ImageManagerStatic as Image;
use FFMpeg\FFMpeg;
use FFMpeg\Coordinate\TimeCode;

function currentUrl( $s = null, $use_forwarded_host = false ) {
    $s = $s ?: $_SERVER;
    $ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
    $sp       = strtolower( $s['SERVER_PROTOCOL'] );
    $protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
    $port     = $s['SERVER_PORT'];
    $port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
    $host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
    $host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
    return $protocol . '://' . $host . $s['REQUEST_URI'];
}

function listDir($path) {
    return preg_grep('/^([^.])/', scandir($path, SCANDIR_SORT_DESCENDING));
}

function generateThumbnail($path, $thumb_path) {
    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
    $thumb_path_dir = dirname($thumb_path);
    if(!file_exists($thumb_path_dir)) {
        mkdir($thumb_path_dir, 0777, true);
    }
    if(is_dir($path)) {
        // Generate directory thumbnail
        $image = Image::canvas(300, 226);

        $dir = listDir($path);
        shuffle($dir);
        $snaps = [];
        // while(count($snaps) < 5) {
        foreach($dir as $name) {
            $pic = $path . '/' . $name;
            if(file_exists($pic)) {
                if(in_array(strtolower(pathinfo($pic, PATHINFO_EXTENSION)), ['jpg','jpeg','png','gif'])) {
                    $snaps[] = $pic;
                }
            }
            if(count($snaps) >= 5) {
                break;
            }
        }
        if(count($snaps) >= 5) {
            $image->insert(Image::make($snaps[0])->widen(150), 'top-left');
            $image->insert(Image::make($snaps[1])->widen(150), 'top-right');
            $image->insert(Image::make($snaps[2])->widen(150), 'bottom-left');
            $image->insert(Image::make($snaps[3])->widen(150), 'bottom-right');
            $image->insert(Image::make($snaps[4])->widen(150), 'center');
        } elseif(count($snaps) > 2) {
            $image->insert(Image::make($snaps[1])->widen(300), 'center');
            $image->insert(Image::make($snaps[0])->widen(150), 'left');
            $image->insert(Image::make($snaps[1])->widen(150), 'right');
        } else {
            $image->insert(Image::make(__DIR__.'/../../public/img/album.png')->widen(300), 'center');            
        }
        $image->save($thumb_path);


    } elseif(in_array($ext, ['jpg','jpeg','png','gif','bmp'])){
        Image::make($path)->widen(300)->save($thumb_path);
    } elseif(in_array($ext, ['mp4'])) {
        FFMpeg::create([
                'ffmpeg.binaries'  => '/usr/bin/ffmpeg',
                'ffprobe.binaries' => '/usr/bin/ffprobe',
            ])->open($path)
            ->frame(TimeCode::fromSeconds(3))
            ->save($thumb_path);
        Image::make($thumb_path)->widen(300)->save($thumb_path);
    } else {
        // If it's not a supported thumb format, use original
        copy($path, $thumb_path);
    }
}
