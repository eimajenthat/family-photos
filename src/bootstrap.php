<?php
require __DIR__.'/../vendor/autoload.php';

//@TODO Namespace this stuff
include __DIR__.'/../src/lib/utilities.php';
$config = include(__DIR__.'/../config/config.php');

// Run checks on config
if(!is_dir($config['album_library'])) {
    die('ERROR: Invalid album library path specified.');
}
if(!is_dir($config['thumb_library'])) {
    if(!mkdir($config['thumb_library'], 0777, true)) {
        die('ERROR: Invalid thumb library path specified: '.$config['thumb_library']);
    }
}
