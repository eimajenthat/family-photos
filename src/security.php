<?php
if (!isset($_SERVER['PHP_AUTH_USER'])) {
    header('WWW-Authenticate: Basic realm="Adams Family Photos"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'You must login to see this page.';
    exit;
} else {
    $authed = true;
    if($config['username'] !== $_SERVER['PHP_AUTH_USER']) {
        $authed = false;
    }
    if($config['password'] !== $_SERVER['PHP_AUTH_PW']) {
        $authed = false;
    }
    if(!$authed) {
        header('WWW-Authenticate: Basic realm="Adams Family Photos"');
        header('HTTP/1.0 401 Unauthorized');
        echo 'Login failed.';
        exit;
    }
}
