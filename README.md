# Adams Photo Site

A simple web site to share family photos.  Reads images, videos, and directories from a given library path.  Generates thumbnails.

Requires libav-tools and ffmpeg to generate video thumbnails:

```
  sudo apt-get install libav-tools ffmpeg
```

Requires exiftran to automatically rotate images and jpegoptim to optimize jpeg files:

```
  sudo apt-get install exiftran jpegoptim
```

Background tasks are handled by cron.  Add the following to your crontab:

```
  * * * * * php [PROJECT ROOT]/scripts/cron.php 1>> /dev/null 2>&1
```
